<?php require_once "./code.php"; ?>

<?php
	$tasks = ["Get Git", "Bake HTML", "Eat CSS", "Learn PHP"];

	if(isset($_GET["index"])){
		$indexGet = $_GET["index"];
		echo "The retrieved task from GET is $tasks[$indexGet]. <br>";
	}

	if(isset($_POST["index"])){
		$indexPost  = $_POST["index"];
		echo "The retrieved task from POST is $tasks[$indexPost]. <br>";
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S04 ACTIVITY</title>
</head>
<body>

	<h1>Task index from GET</h1>
			<form method="">
				<select name="index" required>
					<option value="0">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
				</select>

				<button type="">GET</button>
			</form>

			<h1>Task index from POST</h1>
			<form method="POST">
				<select name="index" required>
					<option value="0">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
				</select>

				<button type="submit">POST</button>
			</form>


	<!-- // Activity -->

	<h1>Building</h1>
	<?php $building = new Building("Caswynn Building",8,"Timog Avenue, Quezon City, Philippines"); ?>

	<p>The name of the building is <?= $building->getName(); ?>.</p>
	<p>The <?= $building->getName(); ?> has <?= $building->getFloors(); ?> floors.</p>
	<p>The <?= $building->getName(); ?> is located at <?= $building->getAddress(); ?>.</p>

	<?php $building->setName("Caswynn Complex"); ?>
	<p>The name of the building has been changed to <?= $building->getName(); ?>.</p>



	<h1>Condominium</h1>
	<?php $condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines"); ?>

	<p>The name of the condominium is <?= $condominium->getName(); ?>.</p>
	<p>The <?= $condominium->getName(); ?> has <?= $condominium->getFloors(); ?> floors.</p>
	<p>The <?= $condominium->getName(); ?> is located at <?= $condominium->getAddress(); ?>.</p>

	<?php $condominium->setName("Enzo Tower"); ?>
	<p>The name of the condominium has been changed to <?= $condominium->getName(); ?>.</p>

</body>
</html>